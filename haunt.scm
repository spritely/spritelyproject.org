;;; spritelyproject.org website
;;; Copyright © 2016-2020 Christine Lemmer-Webber <cwebber@dustycloud.org>
;;;
;;; Site code and contents dual licensed under CC BY 4.0 and Apache v2.

(use-modules (ice-9 match)
             (srfi srfi-1)
             (srfi srfi-11)
             (haunt asset)
             (haunt html)
             (haunt site)
             (haunt page)
             (haunt post)
             (haunt utils)
             (haunt builder blog)
             (haunt builder atom)
             (haunt builder rss)
             (haunt builder assets)
             (haunt reader)
             (haunt reader commonmark)
             (web uri)

             (markdown))


;;; Utilities
;;; ---------

(define %site-prefix (make-parameter ""))

(define (prefix-url url)
  (string-append (%site-prefix) url))


;;; Templates
;;; ---------

(define (stylesheet name)
  `(link (@ (rel "stylesheet")
            (href ,(prefix-url (string-append "/static/css/" name ".css"))))))

(define (base-image image alt)
  `(img (@ (src ,(prefix-url (string-append "/static/images/" image)))
           (alt ,alt))))

(define (header-menu)
  `(("subscribe" ,(prefix-url "/#subscribe"))
    ("archive" ,(prefix-url "/archive/"))
    ("about" ,(prefix-url "/about/"))
    ("contact" ,(prefix-url "/contact/"))))

(define* (base-tmpl site body
                    #:key title #;big-logo
                    [homepage? #f])
  `((doctype "html")
    (html
     (head
      (meta (@ (charset "utf-8")))
      (title ,(if title
                  (string-append title " -- Spritely")
                  (site-title site)))
      ;; css
      ,(stylesheet "main")
      ,(stylesheet "code")
      ;; atom feed
      (link (@ (rel "alternate")
               (title "Spritely news")
               (type "application/atom+xml")
               (href ,(prefix-url "/feed.xml")))))
     (body
      (div (@ (class "main-wrapper"))
           (header (@ (id "site-header"))
                   ;; Site logo
                   (div (@ (class "header-logo-wrapper"))
                        (a (@ (href ,(prefix-url "/"))
                              (class "header-logo"))
                           ,(base-image "spritely-logo-transparent-500px.png"
                                        #;(if big-logo
                                        "fossandcrafts-logo-500px.png"
                                        "fossandcrafts-logo-300px.png")
                                        "Spritely logo"))))
           (div (@ (class "site-main-content"))
                ,body
                ;; ,(support-box #:homepage? homepage?)
                )
           ;; TODO: Link to source.
           (div (@ (class "footer"))
                (a (@ (href "https://gitlab.com/spritely/spritelyproject.org"))
                   "Site contents")
                " released under "
                (a (@ (href "https://creativecommons.org/licenses/by/4.0/"))
                   "Creative Commons Attribution 4.0 International")
                ".  Powered by "
                (a (@ (href "https://dthompson.us/projects/haunt.html"))
                   "Haunt")
                "."))))))

(define* (support-box)
  `(div (@ (id "sponsorship")
           (class "content-box")
           (style "margin-top: 20px"))
        (h1 "✩ Support for the Spritely Project ✩")
        (p "Spritely has been supported thanks to financial support "
           "from the following organizations: ")
        (div (@ (style "display: flex; justify-content: center; flex-wrap: wrap; align-items: center;"))
             (a (@ (href "https://nlnet.nl/PET/"))
                (img (@ (src "/static/images/logo_nlnet-scaled.png")
                        (alt "NLNet")
                        (class "gesso-rounded-offwhite")
                        (style "max-width: 450px; margin: 8px;"))))
             (a (@ (href "https://www.ngi.eu/ngi-projects/ngi-zero/"))
                (img (@ (src "/static/images/ngi_zero-cropped.png")
                        (alt "NGI Zero")
                        (class "gesso-rounded-offwhite")
                        (style "max-width: 450px; margin: 8px;"))))
             (a (@ (href "https://dustycloud.org/blog/samsung-stack-zero-grant/"))
                (img (@ (src "/static/images/samsung-next-stack-zero.png")
                        (alt "Samsung Next Stack Zero")
                        (class "gesso-rounded-offwhite")
                        (style "max-width: 100%; margin: 8px"))))
             (a (@ (href "https://dustycloud.org/blog/samsung-stack-zero-grant/"))
                (img (@ (src "/static/images/samsung-next-stack-zero.png")
                        (alt "Samsung Next Stack Zero")
                        (class "gesso-rounded-offwhite")
                        (style "max-width: 100%; margin: 8px"))))
             (a (@ (href "https://fossandcrafts.org/"))
                (img (@ (src "/static/images/F_and_C_logo_combined-300px.png")
                        (alt "FOSS & Crafts")
                        (class "gesso-rounded-offwhite")
                        (style "max-width: 100%; margin: 8px")))))))

(define* (post-template post #:key post-link)
  (define enclosures
    (reverse (post-ref-all post 'enclosure)))
  `(div (@ (class "content-box blogpost"))
        (h1 (@ (class "title"))
            ,(if post-link
                 `(a (@ (href ,post-link))
                     ,(post-ref post 'title))
                 (post-ref post 'title)))
        (div (@ (class "post-about"))
             (span (@ (class "by-line"))
                   ,(post-ref post 'author))
             " -- " ,(date->string* (post-date post)))
        (div (@ (class "post-body"))
             ,(post-sxml post))))

(define (post-uri site post)
  (prefix-url
   (string-append "/news/" (site-post-slug site post) ".html")))

(define (collection-template site title posts prefix)
  ;; In our case, we ignore the prefix argument because
  ;; the filename generated and the pathname might not be the same.
  ;; So we use (prefix-url) instead.
  `((div (@ (class "episodes-header"))
         (h3 "recent episodes"))
    (div (@ (class "post-list"))
         ,@(map
            (lambda (post)
              (post-template post #:post-link (post-uri site post)))
            posts))))

(define spritely-haunt-theme
  (theme #:name "The Spritely Project"
         #:layout
         (lambda (site title body)
           (base-tmpl
            site body
            #:title title))
         #:post-template post-template
         #:collection-template collection-template))

;; Borrowed from davexunit's blog
(define (first-paragraph post)
  (let loop ((sxml (post-sxml post))
             (result '()))
    (match sxml
      (() (reverse result))
      ((or (('p ...) _ ...) (paragraph _ ...))
       (reverse (cons paragraph result)))
      ((head . tail)
       (loop tail (cons head result))))))

(define* (element-join items #:key [delim " "])
  (let lp ((items items)
           (count 0))
    (match items
      ('() '())
      ((item rest ...)
       (cond
        ((zero? count)
         (cons item (lp rest (1+ count))))
        (else
         (cons delim (cons item (lp rest (1+ count))))))))))

(define (post-preview post site)
  `(li (a (@ (href ,(post-uri site post)))
          (h2 (@ (style "text-align: left; margin: .3em;"))
              ,(post-ref post 'title)))
       (div (@ (class "news-feed-content"))
            (div (@ (class "news-feed-item-date"))
                 ,(date->string* (post-date post)))
            ,(first-paragraph post)
            (div (@ (class "consume-more-buttons"))
                 (a (@ (href ,(post-uri site post)))
                    "[Read more ==>]")))))


;;; Pages
(define (index-content site posts)
  `(div
    ;; Main intro
    (div (@ (class "content-box bigger-text")
            (style "margin-top: 20px; margin-bottom: 20px;"))
         (h1 "✩ Press Start To Take Back The Net! ✩")

         (a (@ (href "https://spritely.institute"))
            (img (@ (src "/static/images/spritely-institute-goblin-and-rocket-350px.png")
                    (alt "Goblin and Rocket"))))

         (p (@ (style "text-align: center; font-style: italic;"))
            "Current development of Spritely is happening over at the "
            (a (@ (href "https://spritely.institute"))
               "Spritely Institute")
            "! "
            "In the meanwhile, the page below contains useful historical "
            "information, but "
            (a (@ (href "https://spritelyproject.org"))
               "spritelyproject.org")
            " will be migrating incrementally over time. "
            "Thanks for your patience!")

         (hr (@ (style "border-top: 2px dashed #675f68; border-bottom: 0px; clear: both;")))

         (p "Spritely is a project to level up the federated social web. "
            "It builds on our experience from co-authoring "
            (a (@ (href "https://www.w3.org/TR/activitypub/"))
               "ActivityPub")
            ", the largest decentralized social network on the web to date, while "
            "applying lesser known but powerful ideas from the "
            (a (@ (href "https://en.wikipedia.org/wiki/Capability-based_security"))
               "object capability security")
            " community.")

         (p "Spritely consists of a number of modular components bringing new and rich "
            "features, from distributed programming, to decentralized storage, to "
            "virtual worlds.")

         (p "Better worlds await, because better worlds are possible.  "
            "We all deserve freedom of communication. "
            "Why not make the journey fun in the process?")

         (hr (@ (class "pre-follow-us")))
         (p (@ (class "follow-us"))
            (span (@ (style "padding-right: 1.5em;"))
                  (b "Follow us: ")
                  (a (@ (href "https://octodon.social/@spritelyproject"))
                     "[fediverse]")
                  " "
                  (a (@ (href "https://twitter.com/spritelyproject"))
                     "[twitter]"))
            " ~ ♥ ~ "
            (span (@ (style "padding-left: 1.5em;"))
                  (b "IRC chat: ")
                  "#spritely on "
                  (a (@ (href "https://libera.chat"))
                     "libera.chat"))))

    (div (@ (class "homepage-video-box"))
         (iframe (@ (width "800")
                    (height "500")
                    (sandbox "allow-same-origin allow-scripts allow-popups")
                    (frameborder "0")
                    (allowfullscreen "true")
                    (src "https://conf.tube/videos/embed/18aa2f92-36cc-4424-9a4f-6f2de946fbd2"))))

    (div (@ (class "content-box"))
         (h1 "✩ Meet the Spritely Family ✩")

         (p "Our goals are ambitious but all of the components are "
            "built on well explored (if sometimes obscure) designs. "
            "To keep things manageable (and not totally vaporware), we're taking "
            "a layered approach and publishing demos as we go.")

         ,(family-entry "goblins" "Goblins: Distributed Programming" #f
                        `((p (a (@ (href "https://gitlab.com/spritely/goblins"))
                                "Goblins")
                             " makes writing secure distributed programs easier and safer, "
                             "even in a mutually suspicious network. "
                             "Its quasi-functional nature gives it easy transactionality "
                             "and even time travel.  (Yes, "
                             (a (@ (href "https://dustycloud.org/blog/goblins-time-travel-micropreview/"))
                                "time travel")
                             "!)")
                          (p "Goblins "
                             (a (@ (href "https://docs.racket-lang.org/goblins/captp.html"))
                                "implements")
                             " CapTP, the "
                             (a (@ (href "http://erights.org/elib/distrib/captp/index.html"))
                                "Capability Transport Protocol")
                             ", "
                             "which has such features as distributed acyclic garbage collection "
                             "and reductions of network round trips through promise pipelining. "
                             "But its biggest feature is how easy it is to write safe "
                             "distributed code, which by following object capability security, "
                             "which mostly resembles normal programming patterns of passing "
                             "around object references.")
                          (p "Goblins is implemented as a "
                             (a (@ (href "https://racket-lang.org/"))
                                "Racket")
                             " library (though a port to "
                             (a (@ (href "https://www.gnu.org/software/guile/"))
                                "Guile")
                             " is planned, and we are hoping for CapTP interoperability with "
                             (a (@ (href "https://agoric.com/"))
                                "Agoric's")
                             " implementation of CapTP) "
                             "and is heavily inspired by the "
                             (a (@ (href "http://www.erights.org/"))
                                "E programming language")
                             ".  It is the most foundational layer of Spritely.")

                          (div (@ (class "homepage-video-box"))
                               (iframe (@ (width "800")
                                          (height "500")
                                          (sandbox "allow-same-origin allow-scripts allow-popups")
                                          (frameborder "0")
                                          (allowfullscreen "true")
                                          (src "https://share.tube/videos/embed/c08afc11-16eb-4d56-8aef-bd19089db65e")))))
                        #:status "Working Beta"
                        #:learn-more
                        `((a (@ (href "https://share.tube/videos/watch/c08afc11-16eb-4d56-8aef-bd19089db65e"))
                             "[Video Intro]")
                          " "
                          (a (@ (href "https://docs.racket-lang.org/goblins/"))
                             "[Docs]")
                          " "
                          (a (@ (href "https://gitlab.com/spritely/goblins"))
                             "[Code]")
                          " "
                          (a (@ (href "https://gitlab.com/dustyweb/terminal-phase"))
                             "[Demo: Terminal Phase]")))

         ,(family-entry "porta-bella" "Porta & Bella: Portable Encrypted Storage" #f
                        `((p "Porta & Bella allow for the storage and distribution of "
                             "files while keeping their contents only known to those who "
                             "have been authorized to access them by receiving the "
                             "appropriate capability URI. "
                             (i "Porta") " refers to our mushroom friend, who provides "
                             "immutable file storage, and " (i "Bella") " refers to the "
                             "vine, who allows for updates by stringing together multiple "
                             (i "Porta") " entries.")
                          (p "In addition to their security properties, Porta & Bella "
                             "allow for portability because the names of such files "
                             "aren't dependent on where they live. "
                             "No matter if the files are kept on your hard drive, on your "
                             "personal server, on a cloud service provider, "
                             "over existing content-addressed systems (like "
                             (a (@ (href "https://ipfs.io/"))
                                "IPFS")
                             "), or stuffed "
                             "in a USB key in a shoebox under your bed (or all of the "
                             "above), the same Porta-Bella URI can refer to all of them.")
                          (p "This means, for instance, that if nodes go down on the "
                             "ActivityPub network but both their contents and profiles "
                             "are stored as Porta-Bella documents, users can easily point "
                             "their account at a new server without a loss of "
                             "functionality or history. "
                             "(See the "
                             (a (@ (href "https://gitlab.com/spritely/golem/blob/master/README.org"))
                                "Golem")
                             " demo as an example.) "
                             "Combining Bella updateable documents with ActivityPub profiles "
                             "is therefore also a practical decentralized identity solution.")
                          (p "Note that Porta and Bella at this point have been prototyped "
                             "and their functionality demonstrated, but this prototype "
                             "is not ready for public use, with their implementations "
                             "subject to change. "
                             "The prototypes went under the names of "
                             (a (@ (href "https://gitlab.com/dustyweb/magenc/blob/master/magenc/scribblings/intro.org"))
                                "Magenc")
                             " and "
                             (a (@ (href "https://gitlab.com/spritely/crystal/blob/master/crystal/scribblings/intro.org"))
                                "Crystal")
                             " and will be renamed soon. "
                             "The Porta-Bella design is derived largely from the "
                             (a (@ (href "https://tahoe-lafs.readthedocs.io/en/tahoe-lafs-1.12.1/"))
                                "Tahoe-LAFS")
                             " project, but a bit tuned more to the needs of Spritely.  "
                             "It is highly likely that "
                             (a (@ (href "https://openengiadina.net/papers/eris.html"))
                                "ERIS")
                             " will be used as the future foundation of this work."))
                        #:status "Prototyped"
                        #:learn-more
                        `((a (@ (href "https://gitlab.com/dustyweb/magenc/blob/master/magenc/scribblings/intro.org"))
                             "[Magenc writeup]")
                          " "
                          (a (@ (href "https://gitlab.com/spritely/crystal/blob/master/crystal/scribblings/intro.org"))
                             "[Crystal writeup]")
                          " "
                          (a (@ (href "https://gitlab.com/spritely/golem/blob/master/README.org"))
                             "[Golem demo]")))

         ,(family-entry "brux" "Brux: Contact Management" #f
                        `((p "If you've ever looked at decentralized identity systems, "
                             "you've probably noticed that their names look like a bunch "
                             "of gibberish letters and numbers. "
                             "Unfortunately this is the tradeoff that decentralized "
                             "identity must take: in order to be globally secure "
                             "and decentralized, we have to sacrifice human-meaningful "
                             "names. "
                             "(See "
                             (a (@ (href "https://en.wikipedia.org/wiki/Zooko%27s_triangle"))
                                "Zooko's triangle")
                             " for the theoretical background of why this is.)"
                             "At first glance, this seems like a real problem, because what "
                             "is the point of our systems if they aren't meaningful to humans?")
                          (p "Brux is a  "
                             (a (@ (href "https://github.com/cwebber/rebooting-the-web-of-trust-spring2018/blob/petnames/draft-documents/petnames.md"))
                                "petnames system")
                             " for Spritely, and it fills this need much the same way the "
                             "contact list on your phone does: you don't need to memorize "
                             "all phone numbers, because your phone provides a locally meaningful "
                             "mapping of names to numbers for you. "
                             "That's exactly what " (i "petnames") " are: locally meaningful names "
                             "mapped onto a global system. "
                             "However " (i "petname systems") " provide more functionality: "
                             "you can leverage your social network to discover other participants "
                             "in the network. "
                             "(It turns out that petname systems also help "
                             (a (@ (href "https://www.w3.org/2005/Security/usability-ws/papers/02-hp-petname/"))
                                "prevent phishing attacks")
                             "... what a great bonus!)")
                          (p "Brux development is in its early stages but we have a "
                             (a (@ (href "https://gitlab.com/spritely/brux"))
                                "functioning prototype")
                             " which integrates with "
                             (a (@ (href "https://gitlab.com/spritely/goblin-chat"))
                                "goblin-chat")
                             ".  We have also written a "
                             (a (@ (href "https://github.com/cwebber/rebooting-the-web-of-trust-spring2018/blob/petnames/draft-documents/petnames.md"))
                                "whitepaper explaining Petnames")
                             " and a (less completed, but analyzed and well outlined) "
                             (a (@ (href "https://github.com/cwebber/rwot9-prague/blob/secure-uis/draft-documents/secure-user-interfaces.md"))
                                "whitepaper on how to build secure user interfaces")
                             " that resemble existing social network UIs."))
                        #:status "Working Pre-Alpha"
                        #:learn-more
                        `((a (@ (href "https://gitlab.com/spritely/brux"))
                             "[Code]")
                          " "
                          (a (@ (href "https://github.com/cwebber/rebooting-the-web-of-trust-spring2018/blob/petnames/draft-documents/petnames.md"))
                             "[Petnames Paper]")))

         ,(family-entry "mandy" "Mandy: Easy ActivityPub Integration" #f
                        `((p "We want to make it easy to hook together programs written "
                             "in Goblins to the existing "
                             (a (@ (href "https://www.w3.org/TR/activitypub/"))
                                "ActivityPub")
                             " speaking federated social network. "
                             "Thankfully this should be a very clean mapping, because both "
                             "Goblins and ActivityPub follow the "
                             (a (@ (href "https://en.wikipedia.org/wiki/Actor_model#Fundamental_concepts"))
                                "classic actor model")
                             ".  Mandy's design is to bridge the worlds of distributed Goblins programs "
                             "and ActivityPub by providing an easy mapping between them.")
                          (p "In the process we can add richer interactions and better protections "
                             "against abusive behavior (and better support for cooperative, "
                             "intentional behavior) than currently exist in the ActivityPub "
                             "ecosystem.  See the "
                             (a (@ (href "https://gitlab.com/spritely/ocappub/blob/master/README.org"))
                                "OcapPub")
                             " writeup for some of these ideas (most of those do not require "
                             "Mandy to implement them, but Mandy will make implementing them "
                             "much easier)."))
                        #:status "Planned")

         ,(family-entry "hyptis" "Hyptis: Distributed Finance" #f
                        `((p "Hyptis provides distributed small-world finance systems, "
                             "appropriate for use in games or for communities such as your "
                             "neighborhood mutual aid network.")
                          (p "Hyptis builds upon "
                             (a (@ (href "https://agoric.com/papers/"))
                                "object capability financial theory")
                             " which is well-researched and easy to implement on top of a "
                             "distributed ocap style system such as Spritely Goblins. "
                             "As said, we're aiming for small-world financial systems "
                             "(though the folks at "
                             (a (@ (href "https://agoric.com/"))
                                "Agoric")
                             " are putting in work on large-world financial systems, and if we "
                             "achieve CapTP interoperability with "
                             "them, their systems should be usable through Spritely as well). "
                             "However, it is amazing how much can be easily done in such a small "
                             "amount of code with Goblins' design: a simple money implementation "
                             "which preserves transactional integrity even in the event of errors "
                             "and allows for account holders to have access from across a network "
                             "can be implemented in as little as "
                             (a (@ (href "https://gitlab.com/spritely/goblins/-/blob/6b31d2340e8c48aa09958c810399962eb558e2d3/goblins/actor-lib/simple-mint.rkt#L13"))
                                "25 lines of code")
                             " in Goblins... no blockchain required! "
                             "(This design is ported directly from the "
                             (a (@ (href "http://erights.org/elib/capability/ode/index.html"))
                                "Capability-based Financial Instruments")
                             " document.)"))
                        #:status "Planned, Small Examples")

         ,(family-entry "oaken" "Oaken: Safely Run Untrusted Code" #f
                        `((p "Wouldn't it be a travesty if someone built an amazing looking "
                             "fire elemental monster that you really want to put in your game, "
                             "but you're afraid that if you add it that it'll upload all your "
                             "passwords and credit card information to some unsavory individuals "
                             "and then proceed to delete all the files on your system?")
                          (p "The general perspective in computing is that if you run untrusted "
                             "code, and that untrusted code wants to do something dangerous "
                             "(whether maliciously or due to unintentional bugs), you're hosed. "
                             "But it doesn't have to be that way.")
                          (p "Oaken has yet to be written, but its plan is based off of the "
                             (a (@ (href "http://wiki.erights.org/wiki/Walnut/Ordinary_Programming/Emakers"))
                                "Emaker design")
                             " from "
                             (a (@ (href "http://www.erights.org/"))
                                "E")
                             " and the "
                             (a (@ (href "https://github.com/tc39/proposal-realms/blob/main/explainer.md"))
                                "Frozen Realms proposal")
                             " for Javascript."))
                        #:status "Planned")

         ,(family-entry "fantasary" "Fantasary: Distributed Virtual Worlds" #f
                        `((p "Here's where Spritely becomes extra fun... where social "
                             "networks meet games! "
                             "After all, modern social networks are merely degenerate versions "
                             "of virtual worlds... you can chat and send messages in both, "
                             "but you can't go solve ghost mysteries with your friends or "
                             "fight monsters or hand your friend a drink that makes them dizzy "
                             "when they drink it. "
                             "These days we have distributed social networks and virtual worlds, "
                             "but no well developed combination thereof. "
                             "Fantasary aims to change that, while returning a sense of fun "
                             "and collaboration to online spaces. "
                             "By building worlds with your friends, Fantasary "
                             "should provide an environment to explore fun environments and also "
                             "explore learning to program as an educational and cooperative "
                             "tool. "
                             "(Not to mention getting secure communication tools in the hands "
                             "of users in a way that seems fun!)")
                          (p "This is admittedly both the most complex and ambitious piece of "
                             "the Spritely system (though also the most exciting). "
                             "However, it is possible to be built; in the mid-to-late 1990s, "
                             (a (@ (href "https://www.youtube.com/watch?v=KNiePoNiyvE"))
                                "Electric Communities Habitat")
                             " did exactly this, but the company closed before it achieved all "
                             "of its goals, and it was a proprietary system (whereas of course "
                             "everything in Spritely is free and open source software).")
                          (p "But this is also a good example of why building something this "
                             "ambitious is worthwhile: as a driver for powerful and interesting "
                             "technology. "
                             "After Electric Communities Habitat closed, its ideas lived on in the "
                             (a (@ (href "http://www.erights.org/"))
                                "E programming language")
                             ", which we have already said that Goblins is built upon "
                             "(as are many other ideas in Spritely). "
                             "Thus in this case, even failure can be a path to success if it advances "
                             "the tooling for distributed social networks in general.")
                          (p "Two demos are planned for Fantasary to come out between 2021 and 2022: "
                             "a textual virtual world demo (similar to but more sophisticated than the "
                             (a (@ (href "https://archive.org/details/feb_2017-live_network_coding_8sync"))
                                "mudsync demo")
                             ", which was built for a Goblins predecessor) and a graphical system that "
                             "uses assets following the "
                             (a (@ (href "https://lpc.opengameart.org/static/lpc-style-guide/index.html"))
                                "Liberated Pixel Cup style guide")
                             " (see the "
                             (a (@ (href "https://github.com/makrohn/Universal-LPC-spritesheet"))
                                "Universal LPC Spritesheet")
                             " for updated assets and the "
                             (a (@ (href "https://opengameart.org/art-search?keys=lpc"))
                                "vast collection of compatible assets")
                             ")."))
                        #:status "Planned")

         ,(family-entry "aurie" "Aurie: Serialize and Restore the World" #f
                        `((p "So, Goblins has "
                             (a (@ (href "https://dustycloud.org/blog/goblins-time-travel-micropreview/"))
                                "time travel")
                             "... great!  But that's really for within the live runtime.  "
                             "What happens if we want to save the whole state of the world "
                             "and its objects and bring it back later? "
                             "And how can we do it securely, especially when objects "
                             "might want to claim that the next time they're reconstructed "
                             "that they have more power and authority than they actually did? "
                             "(Oh yeah... didn't you know I'm the king, and I have a magic "
                             "scepter that allows me to banish anything I point it at, and "
                             "also I have one bajillion pieces of gold in my account? "
                             "Yes, yes...)")
                          (p "By following the patterns laid out in "
                             (a (@ (href "http://erights.org/data/serial/jhu-paper/index.html"))
                                "Safe Serialization Under Mutual Suspicion")
                             ", it should be possible to serialize, restore, and upgrade "
                             "graphs of objects and their relationships "
                             "which are dynamically generated as the program runs. "
                             "No need to fear about everything falling apart if your game "
                             "server crashes... just spin it back up!"))
                        #:status "Working Pre-Alpha"
                        #:learn-more
                        `((a (@ (href "https://gitlab.com/spritely/aurie"))
                             "[Code]")))

         ,(family-entry "questie" "Questie: Distributed Debugging" #f
                        `((p "Okay, so you've built a complicated networked program... "
                             "but now all these messages are flying around the network and you "
                             "can't tell what is causing what anymore! "
                             "Questie solves this with a design highly inspired by the "
                             (a (@ (href "http://wiki.erights.org/wiki/Causeway"))
                                "Causeway message-oriented distributed debugger")
                             " (more: "
                             (a (@ (href "https://www.youtube.com/watch?v=QeqcGa7HlBk"))
                                "a wonderful screencast")
                             ", "
                             (a (@ (href "https://www.hpl.hp.com/techreports/2009/HPL-2009-78.html"))
                                "tech report")
                             ", "
                             (a (@ (href "http://www.erights.org/elang/tools/causeway/index.html"))
                                "erights page")
                             "). "
                             "Thanks to Goblins' design, we should also be able to add some nice "
                             "features of being able to snapshot the running state of the program "
                             "at relevant points to let developers explore the local state of the "
                             "world the time in question."))
                        #:status "Planned"))

    ;; TODO: News updates and etc
    (div (@ (class "content-box homepage-news-box")
            (style "margin-top: 20px; margin-bottom: 20px;"))
         (h1 "✩ News! ✩")
         (ul (@ (class "homepage-news-items"))
             ,@(map (lambda (post)
                      (post-preview post site))
                    (take-up-to 10 (posts/reverse-chronological posts))))
         (p (@ (style "text-align: center"))
            (a (@ (href "/archive/"))
               "[--archive--]")))
    ,(support-box)
    ))

(define* (family-entry name-lower name-title maybe-link
                       contents
                       #:key (status #f)
                       (learn-more #f)
                       )
  `(div (@ (class "family-entry")
           (id ,name-lower))
        (div (@ (class "title-wrap"))
             (h2 ,name-title
                 ,@(if status
                       `((span (@ (style "float: right"))
                               " (Status: "
                               ,status
                               " )"))
                       '())))
        (div (@ (class "family-entry-bodywrap"))
             (div (@ (class "family-portrait-wrap"))
                  (img (@ (class "family-portrait")
                          (src ,(prefix-url (string-append "/static/images/sprites/"
                                                           name-lower
                                                           "-150x150.png")))
                          (alt ,(string-append name-lower " family portrait")))))
             (div (@ (class "family-entry-content"))
                  ,@contents))
        ,@(if learn-more
              `((div (@ (class "learn-more"))
                     (hr (@ (class "pre-learn-more")))
                     (b "Learn more: ")
                     ,@learn-more))
              '())))


(define (archive-content site posts)
  `(div (@ (class "content-box bigger-text")
	   (style "margin-top: 20px; margin-bottom: 20px;"))
        (h1 "✩ News Archive ✩")
        (ul (@ (class "homepage-news-items"))
            ,@(map (lambda (post)
                     (post-preview post site))
                   (posts/reverse-chronological posts)))))

(define (index-page site posts)
  (make-page
   "index.html"
   (base-tmpl site
              (index-content site posts)
              #:homepage? #t
              ;; #:big-logo #t
              )
   sxml->html))

(define (archive-page site posts)
  (make-page
   "archive/index.html"
   (base-tmpl site
              (archive-content site posts))
   sxml->html))

;;; Site

(site #:title "Spritely"
      #:domain "spritelyproject.org"
      #:default-metadata
      '((author . "Spritely Project")
        (email  . "spritely@dustycloud.org"))
      #:readers (list ;; skribe-reader
                      commonmark-reader*)
      #:builders (list (blog #:theme spritely-haunt-theme
                             #:prefix "/news")
                       (atom-feed #:blog-prefix "/news")
                       (atom-feeds-by-tag #:blog-prefix "/news")
                       (rss-feed #:blog-prefix "/news")
                       index-page
                       archive-page
                       (static-directory "static" "static")
                       #;(rss-feeds-by-tag)))
