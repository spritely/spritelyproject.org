title: Spritely website launches!
date: 2020-09-29 14:30
author: Christine Lemmer-Webber
tags: 
---
Whew, we're happy to finally say that
[https://spritelyproject.org/](https://spritelyproject.org/) exists and
is up and running!
(Well, supposedly you know because you're looking at it right now!)

Spritely was initially vaguely announced in an
[interview over at We Distribute](https://medium.com/we-distribute/faces-of-the-federation-christopher-allan-webber-on-mediagoblin-and-activitypub-24bbe212867e)
about two and a half years ago, not long after
[ActivityPub became a W3C Recommendation](https://dustycloud.org/blog/activitypub-is-a-w3c-recommendation/).
Not long after I made a [similar announcement on my blog](https://dustycloud.org/blog/spritely/),
followed quickly by [the announcement](https://dustycloud.org/blog/announcing-goblins/)
of Spritely's first (and most foundational) subproject,
[Spritely Goblins](https://docs.racket-lang.org/goblins/index.html).
[Development on Goblins](https://gitlab.com/spritely/goblins)
proceeded at a quick pace, and the first demos of how Spritely's
encrypted portable storage layer (Porta-Bella) followed quickly after.

So why then did it take so long to make and announce a website?
Quite simply, it's because I wasn't ready to make this a community
project quite yet because there were too many ideas that were outside
the mainstream.
It turns out that Spritely isn't doing many things that are *new*,
but it is doing many things that are *relatively unknown*, and at the
start, even I didn't fully understand them.
Thanks to the help of the object capability security community, I
gained enough knowledge and confidence where I now have a pretty good
idea how each layer works.
(Spritely's goals simply can't be achieved in anything other than an
object capability architecture.)
I wanted to be confident that we could do everything we were claiming
we would before making bold claims in a very public way.

Furthermore, I needed to get the foundational layers down enough where
I could invite others to start experimenting and building.
Now that [Spritely Goblins](https://docs.racket-lang.org/goblins/index.html)
has the beginnings of support for distributed networked programming,
I think that we're finally at that point.

There's a lot more to do... we've really just gotten started.
As Spritely's subtitle goes, "Social Worlds Await"... but only if we
make them happen... back to work!
