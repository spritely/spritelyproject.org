title: State of Spritely, December 2020
date: 2020-12-01 16:30
author: Christine Lemmer-Webber
tags: state-of, goblins
---
![Spritely characters sitting campfire-style around a newly formed universe](/static/images/blog/2020-10-31_spritely_scene.jpg)

It's been a bit since we've had a chance to get an update out.
In fact, my intention was to get out a blogpost about a month ago to
coincide with the most excellent artwork above made by
[David Revoy of Pepper and Carrot](https://www.peppercarrot.com/)
(who now has some
[wonderful hardcover books you can buy](https://www.davidrevoy.com/static9/shop)
if you like this style of artwork).
However, enough other stuff had gotten in a way that it took me a bit
to clear time to write this post.
Despite that, there's much to say!

I have given a talk at [RacketCon](https://con.racket-lang.org/) this
year [introducing Spritely Goblins core concepts](https://share.tube/videos/watch/c08afc11-16eb-4d56-8aef-bd19089db65e).
The [homepage entry about Goblins](/#goblins) now features this video.

Also reflected in changes on the homepage is that [Aurie](/#aurie),
Spritely's running-system-serialization system, has been updated from
"Planned" to "Working Pre-Alpha" status and its
[corresponding code](https://gitlab.com/spritely/aurie)
is linked to at the bottom of the entry.
Yes, code now exists!

I'm also working on a demo for it, but it's a bit tied up in another
project... the
[Fantasary virtual worlds](https://spritelyproject.org/#fantasary)
sub-project!
Yes, some progress is being made towards that, and yes, the use of
Aurie + Fantasary is so that running game worlds can be serialized and
then later restored!

So, unreleased, but I have a version of this (textual virtual worlds,
not yet graphical) where you can walk around, make rooms, look at rooms
and save that world... but it's not very interesting.
Nonetheless, I might add a bit to it, clean it up, and release it as a
demo.
It's nice to show, at least, that the system works.

Part of the reason this hasn't been released yet is that I've gotten
distracted thinking long-term about what the input system is like.
Due to the kinds of worlds we want to build, *secure* text-input is
necessary, while also being sufficiently comfortable to use.
This is trickier to solve than it sounds, but you can see my
[rough plan for a textual world input system](https://groups.google.com/g/cap-talk/c/Wa6VR-k3bLo)
on
[cap-talk](https://groups.google.com/g/cap-talk), which is where
ocap design conversations tend to happen.

However, since shiny things are always fun, I can give a preview of an
early GUI mockup of the virtual worlds interface which was done as
part of the ActivityPub Conference hackathon:

![Fairy forest mockup](https://dustycloud.org/gfx/goodies/fairy-forest-ui-mockup2.png)

But in order to truly deliver on the virtual worlds demo, the
following things need to happen:

 - I need to actually add support for long-lived ocap "URIs", also
   known as "sturdyref URIs".
   These are links you can copy and paste around to give access to
   enter a world, etc.

 - However, I also need to work on support for CapTP connections which
   have more than two parties connected at once.  This is more or less
   known as adding support for "handoffs" in Goblins.

 - Then I need to implement the input syntax system I've planned...
 
 - And then we actually need to build a fun little demo virtual world.
 
All of that is planned and in progress.

Hope that helps better explain where things are at.
I think once the virtual worlds examples start to appear, onboarding
new contributors to Spritely will be a bit easier; there will be
something fun to play with.
But more news soon!
