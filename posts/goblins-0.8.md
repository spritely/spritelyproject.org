title: Spritely Goblins v0.8 released!
date: 2021-07-18 17:10
author: Christine Lemmer-Webber
slug: goblins-0.8
tags: goblins
---
We are happy to announce the release of
[Goblins](https://spritelyproject.org/#goblins)
[v0.8](https://gitlab.com/spritely/goblins/-/tree/v0.8)!
Goblins is a distributed actor model system with localized transactions
layered on top of [Racket](https://racket-lang.org/).
See the [documentation](https://docs.racket-lang.org/goblins/index.html)
for usage, and the
[ChangeLog](https://gitlab.com/spritely/goblins/-/blob/v0.8/CHANGELOG.org)
for what's changed in this release!

This release has been some time in coming, so it's a good idea to
review what has happened since.
The biggest feature in this release is advancements in
[Goblins' CapTP implementation](https://docs.racket-lang.org/goblins/captp.html),
particularly the introduction of "handoffs".
See the ["What is CapTP" blogpost](https://spritelyproject.org/news/what-is-captp.html)
which explains more about this and other ideas (and has a nifty, if
befuddling looking, whiteboard diagram).
We also now ship a layer for easy integration between peer to peer
nodes over Tor Onion Services.
[goblin-chat](https://gitlab.com/spritely/goblin-chat) has been updated
with an example.

We also have some neat new features surrounding support for
[opt-in coroutines](https://gitlab.com/spritely/goblins/-/blob/master/goblins/actor-lib/await.rkt)
for a specific actor which compose nicely with the existing designs
for "promise pipelining" in Goblins, but pleasantly do not have the
general risk of "plan interference" that many coroutine systems suffer
from (a near actor performing immediate calls lower on the stack of a
vat cannot be interrupted by an actor higher on the the stack; this is
implemented via "continuation barriers").
We also have, at last,
[pleasant integration with Racket's existing synchronizable events](https://gitlab.com/spritely/goblins/-/blob/master/goblins/actor-lib/sync-pr.rkt),
simplifying integration of Goblins and other IO systems.
More features and bugfixes as well; see the
[ChangeLog](https://gitlab.com/spritely/goblins/-/blob/v0.8/CHANGELOG.org) for
the more significant ones.

What you may have noticed is that in some of the above mentioned
new features, we lack proper documentation.
This is true and something we could use help with.
If you are interested in helping but would like guidance, the best way
is to ping `cwebber` in the `#spritely` IRC channel on
`irc.libera.chat`.

A bit of a tangent from the release itself, we have begun
collaborating on the [OCapN project](https://github.com/ocapn/ocapn)
with the developers from [Agoric](https://agoric.com/), [CapN
Proto](https://capnproto.org/), to bring a unified new generation of
CapTP.  This is, of course, a thing that needs to prove itself by
being played out in practice.

The main thing missing from the current networking tooling with CapTP,
OCapN, etc is a way to subscribe to and react in response to severed
network sessions.
We hope to have this sorted in the next release.

Aside from that... our hope is that the next few months will bring
demonstrative examples of tools built *on top of* Spritely Goblins.
So... we'd better get back to hacking!

See you next time!
