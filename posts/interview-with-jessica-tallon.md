title: Jessica Tallon joins with a grant from NLNet/NGI Zero!  Plus an interview!
date: 2021-11-10 16:50
author: Christine Lemmer-Webber
slug: interview-with-jessica-tallon
tags: tsyesika, interview
---
![Jessica Tallon standing in front of some trains](/static/images/blog/tsyesika-and-trains-edit.jpeg)

We have some good news, and it comes in two parts!
The first part is that Spritely has received its second grant
from [NLNet](https://nlnet.nl/PET/) and
[NGI Zero](https://www.ngi.eu/ngi-projects/ngi-zero/)!
(You may remember that the first was from the
[Content Addressed Descriptors and Interfaces](https://spritelyproject.org/news/content-addressed-descriptors-paper.html)
grant and corresponding paper, which ended up funding most
of Goblins' [implementation of CapTP](https://spritelyproject.org/news/what-is-captp.html)!)
The second part is who that means we've brought on to do it:
Jessica Tallon, with whom I
[co-authored and co-edited ActivityPub](https://www.w3.org/TR/activitypub/)!
Jessica and I have a long history of collaboration; a crowdfunding
campaign from MediaGoblin allowed us to
[hire Jessica to work on federation](https://mediagoblin.org/news/welcome-jessica-tallon.html),
and in many ways that kicked off both of our involvement in the
ActivityPub spec, and before then Jessica was in intern in MediaGoblin
through Outreach Program for Women (the predecessor to
[Outreachy](https://www.outreachy.org/)).

But Jessica has more to say about her own history, and we have an
interview about that below!
So I'll just wrap up by saying that Jessica is already picking up
Spritely's tooling *extremely* fast, and it's nice to have another
dedicated collaborator I already strongly trust and work well
with.

With no further ado, let's hop into the interview!

**CLW (Christine Lemmer-Webber):** Hi Jessica, thanks for joining us on this interview today!

**JLT (Jessica L. Tallon):** Hey! Thanks for having me, it's nice to be here.

**CLW:** So maybe not everyone here knows much about you, would you like to introduce yourself?

**JLT:** I'm Jessica Tallon. I've worked on many things over the years, but my history of working in this space is I [helped implement](https://mediagoblin.org/news/jessica-tallon-wrapping-up-opw.html) a [precursor to ActivityPub in mediagoblin](https://mediagoblin.org/news/mediagoblin-0.8.0-gallery-of-fine-creatures.html) ([the pump.io federation API](https://github.com/pump-io/pump.io/blob/master/API.md)) while helping co-author the [ActivityPub specification](https://www.w3.org/TR/activitypub/) at the [W3C](https://www.w3.org/). Since then I've been working at [Igalia](https://www.igalia.com/) on a number of things including networking, accessibility and compilers.

**CLW:** Yes, and it's really great to be working together again... on that note, since the exciting news here is that you've got this grant so that you can focus your work on contributing to Spritely, would you like to tell us what the grant is about?

**JLT:** Sure! The grant is implementing a petnames system and demoing it in [Goblin Chat](https://gitlab.com/spritely/goblin-chat).  Petnames systems provide a mechanism for supporting human-meaningful naming on top of decentralised naming systems, and work nicely with ordinary human development of trust, and even provide some robustness against things like phishing attacks. The proposal for [a petname system](https://github.com/cwebber/rebooting-the-web-of-trust-spring2018/blob/petnames/draft-documents/petnames.md) separates the names into three different types:

- Self proposed names (this is a name one gives oneself, generally not preferred to display when other names are available, but still important)
- Petnames (these are names you give to your own contacts - think of your contacts app on your phone)
- Edge names (these are names suggested to you by your contacts)

These different types of names are represented in a visually different way which means that if someone has a petname you have picked out for them, no one can set a self-proposed name to impersonate them because you will see it's self-proposed and know you cannot trust it. This also allows you to suggest names for your contacts in a way which tells them who is contacting them and who is suggesting the name that is being proposed.

![Searching through a petname database](/static/images/blog/search-interface.svg)

**CLW:** So this grant is really aiming for three things: to build out a petname system on top of [Goblins](https://spritelyproject.org/#goblins) (the distributed programming system) we ourselves can use in Spritely ([Brux!](https://spritelyproject.org/#brux)), to demonstrate to others how a petname system would work, and also to push forward the social networking plans in Spritely... on that note, you're kind of at the forefront of trying out a lot of the components that I've been developing in research-mode for the last few years.  So I guess as the official development lab rat of the Spritely project, how's the experience been for you?

**JLT:** The  experience has actually been really good. I expected things to be in a rougher state than they are.

**CLW:** High praise huh?

**JLT:** I think with a project the size and scale of Spritely it'd be natural for it to be in a more lab/development state than it is. I think the Goblins tutorial especially is really good and fairly comprehensive, which I think for a project which has mostly been in a heavy development phase is pretty impressive.

**CLW:** Thanks, I'll take that praise!  Well, there are a lot of good ideas in Spritely (and Goblins in particular) that have been borrowed from the object capability security community... a lot of the ideas aren't new, but they're not well known, especially all the ideas that have been borrowed from the [E language](http://www.erights.org/).  Nonetheless, has Goblins been fairly comfortable to program in?

**JLT:** Yes it has been. I think it provides some really good tooling to do some pretty complicated things. If you take the [Goblin Chat](https://gitlab.com/spritely/goblin-chat) example, writing a chat room program which is distributed, end-to-end encrypted, peer-to-peer, and is independent of network layer (e.g. tor) is something that would be really difficult to think about without Goblins and I think it's quite impressive and to take a step back and look at the tooling Goblins gives you to build a wide range of applications. I think if my biggest stumbling block so far has perhaps been the domain-specific terminology e.g. [vats](http://www.erights.org/elib/concurrency/vat.html), [depictions](http://erights.org/data/serial/jhu-paper/index.html), etc.

![Goblins chat GUI demo](https://dustycloud.org/misc/goblins-chat-captp-onion-services.gif)

**CLW:** I'm glad you feel that way... yes, part of the reason that Spritely has taken so long to get to this state is that I wanted to find the right abstractions to make reasoning about and building something as big as the Spritely vision feasible.  And that ended up meaning learning from and borrowing massive amounts of ideas from the object capability (ocap) security community.  Your comment that the most overwhelming thing is the sheer amount of new vocabulary that the ocap world tends to introduce rings true to me and I think everyone I know entering this space.  But it's hard to tell having sat in it for a couple of years... it does seem like those terms exist because there needed to be new terms to explain those ideas, and they're just different ideas... the more you sit with them, the more natural they feel.  But it does feel frustrating I think for beginners.  I don't know, do you think it could be made better just by having more training resources or by immersion or by cutting out some of these terms?  Or do you think it's a tricky problem to avoid?  (I think I've passed the threshold of ocap immersion where I'm not fit to answer this question myself anymore!)

**JLT:** I think this is a tricky problem. You're right that a lot of the ideas don't exist, at least in a widespread fashion outside of this space.  But I wonder if there can be simpler or more intuitive terms.  Like the term ["actor model"](https://en.wikipedia.org/wiki/Actor_model) exists outside of the ocap space, but the word ["vat" for event loops](https://docs.racket-lang.org/goblins/tutorial.html#%28part._.Vats__actors__spawning__and_immediate_calls%29)?  It's strange choice.

**CLW:** Yes well in [8sync](http://www.gnu.org/software/8sync/) and [XUDD](https://xudd.readthedocs.io/en/latest/) (legacy actor-model exploration projects which were predecessors to Spritely Goblins) I had discovered vats, or at least kind of discovered them, independently, and I called them "hives".

**JLT:** Well I thought that was a better term; I don't remember being as confused in XUDD or 8sync when I was looking at it.

**CLW:** Do you know where the term "vat" comes from by the way?

**JLT:** I... don't know?  I'm guessing some kind of... well it's a container, so it contains the actors in the event loop?

**CLW:** I was told it came from "how do you know if you're a brain in a vat?"

**JLT:** ... that's really strange.  I think I like "hive" better.

**CLW:** Well, I did push on this in the ocap community for a while and tried to convince people to use "hive" and there was a lot of pushback from those who had been around.  And eventually they convinced me, because "hive" has its own problems... it sounds like a skin disease symptom, and some people are afraid of bees, etc... and Mark Miller said "sometimes the name of the thing just eventually becomes the name of the thing".  And it's true that the ocap world did a really good job of defining what vats are, and I didn't want to make it hard to find the research literature for people reading these things.  But I guess "vats" aren't the only term... there's ["rights amplification" and "sealers/unsealers"](http://erights.org/elib/capability/ode/ode-capabilities.html), etc.

**JLT:** I actually think "sealers" and "unsealers" are fine, because they're descriptive.  But I think things like "vats" and... well I've already forgotten what "rights amplification" means.

**CLW:** Generally it's for patterns like where you have capabilities "hidden in plain sight", you use unsealers to access sealed capabilities, for doing stuff that's kind of equivalent to the ideas of "groups" in ACLs.  Uh, I think I'm taking this interview off track.

**JLT:** I think my closing remark on this is I do understand not wanting to invent new terms when there's an established community around terms but it is hard as a newcomer, and I don't know if they're the best choice.

**CLW:** But then again, "string" isn't a great name for "text".  But eventually you get used to it.

**JLT:** I think that's true, there are a lot of new ideas here, and terms like "string" aren't intuitive either, but you learn all those things when you learn to program and they're common across programming languages.  But here there a lot of new ideas, and you do need to call those ideas something, but some I have an easier time with than others.  But if ocap programming became *the* way everyone would program, I guess these would be the terms everyone knew.  Like, I'm guessing "class" is a bad name for object-oriented programming, and "object" is a great name.  But that's just a guess.  Maybe I'm getting in too deep.

**CLW:** [What an object is](http://mumble.net/~jar/articles/oo.html) is a fun topic of its own, since that means a lot of things.  But yeah, I guess naming is the ultimate bikeshed.  We should probably move on.

**JLT:** Strong agree.

**CLW:** So I guess what I'd like to ask is: why does Spritely matter to you?  I mean, in a sense you and I already burned several years of our life on standardising ActivityPub, and there's been a easily sympathisable criticism of Spritely of "why introduce new ideas into this landscape, we're already busy with the ActivityPub world as it's currently rolled out".  So what do you think about that?

**JLT:** So to me, I find that Spritely is much bigger than a tool for the social space.  I think it will be a fantastic tool *for* that, however it can be used for so many things.  It feels like this is a new foundational layer for the internet.  There's a myriad of different applications for it, from a distributed password manager to... well, they're not *just* fundamentally social tools.  Like, ActivityPub was trying to target the spaces of like, decentralised Twitter.  And Spritely's ideas apply to distributed social networking, but also... there's a lot of tooling that needs to be collaborative, like collaborative text editors, or shared todo type systems, etc.  And Spritely's ideas allow you to focus on the application task at hand rather than on the messaging specifics, etc.  Like, with Spritely, I can just make all these applications peer-to-peer, and that's the natural way to develop them, which otherwise... before being exposed to this tooling, I just wouldn't bother, I'd just do client-server because it's just way too much to think about and the tooling just isn't there.  So I'm excited about all that and to make opening that up to everyone more possible.

**CLW:** Okay great... well, this has been a fun interview, but you and I actually have a lot of real work to get done!  So do you have any closing remarks?  Things about you, things you're excited about, or what you are thinking about?

**JLT:** I guess I just want to say that I'm really excited to be working on this project, especially for the two of us to be working together.

**CLW:** We do make a good team!

**JLT:** Absolutely.  And I know I have more stuff I want to put on the Spritely blog soon, showing off some of the stuff we're building in progress and explain some of the other ocap ideas, because there's a lot going on here.  So I guess that's it!

**CLW:** Well in that case, thank you for joining us for this interview!

**JLT:** Thanks for having me!
