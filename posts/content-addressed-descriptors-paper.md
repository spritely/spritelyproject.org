title: Content Addressed Descriptors and Interfaces with Spritely Goblins paper
date: 2021-07-30 12:30
author: Christine Lemmer-Webber
slug: content-addressed-descriptors-paper
tags: goblins, paper
---
The paper
[Content Addressed Descriptors and Interfaces with Spritely Goblins](https://dustycloud.org/tmp/interfaces.html)
is now available for reading.
(Also available: [source and examples](https://gitlab.com/spritely/interfaces-writeup),
as well as [PDF](https://dustycloud.org/tmp/interfaces.pdf) and [ODT](https://dustycloud.org/tmp/interfaces.odt) versions.)
It is somewhat of a wide-ranging paper in its explored ideas, but
the general basis is how to perform "conversational" programming
in [Spritely Goblins](https://docs.racket-lang.org/goblins/index.html),
or any other system which assumes a mutually suspicious network.
There are some other interesting ideas in there, including Goblins'
ward/incanter system (a way of installing hidden behavior which requires
hidden access, and then a capability to gain and apply such access).

The reason this system is able to be elegantly embedded in a network environment
is its use of
[Spritely's implementation of CapTP](https://spritelyproject.org/news/what-is-captp.html).
Much of the CapTP implementation, as well as the research related to the paper
itself, have been funded by
[NLnet](https://nlnet.nl/PET/) and [NGI Zero](https://www.ngi.eu/ngi-projects/ngi-zero/).
We are grateful for the support!
