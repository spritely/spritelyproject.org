title: Petname Systems
date: 2022-02-18 17:20
author: Jessica Tallon
slug: petname-systems
tags: goblins, petnames, brux
---
*This blogpost is based on Jessica's work being fudned by [NLNet / NGI Zero](https://spritelyproject.org/news/interview-with-jessica-tallon.html).  We are grateful for the support of this work!*

A [petname system](https://github.com/cwebber/rebooting-the-web-of-trust-spring2018/blob/petnames/draft-documents/petnames.md) is a way of naming people in a way which solves a lot of the problems surrounding trust while keeping them useful and meaningful to you specifically without relying on a centralized naming authority.

To look at how petname systems help with this, we have to look at the typical way naming is handled in in typical application design today. We will then look at how Spritely is applying petname system designs on top of its prototype social networking applications.

# Naming yourself (self proposed names)

You are likely already familiar with systems which support naming yourself; think about joining a forum or a chatroom. You probably are asked to give yourself a username when you join; this is the name people see when you participate.  In a centralized system (like DNS or Twitter), there is only one username permitted for the whole system. However in decentralized, federated systems like email or the federated social web (using webfinger), namespaces are instead used. For example, on "social.example", there can only be one "alice" username, "alice@social.example".  (Some systems also permit a "display name" in addition which are similar for being user-chosen, but are not used for addressing the user and are typically not restricted in case of conflicts.)

These are useful in that they give people the name you want to go by, but they fall down in terms of trust and discoverability. First of all, with people giving themselves their own names, you have to trust that this person is really who they say they are. Even if these names are forced to be unique as they usually are on a forum, you have to trust the forum to not allow someone else to take the name if a user deletes their account. (IRC is even worse in that people can often just join with your username if you're not online at the time they are.) In a fully peer-to-peer system, this is made further difficult in that there is no-one ensuring names are unique or that someone is who they say they are.

# Giving names to others locally (petnames)

The other common naming system we have is names you give other people. This is probably best known in terms of contacts on your phone. Each person has a unique identifier (their phone number) and you can give them a name of your choosing. These names are great in being very meaningful to you personally; for example, if you want you can save your mom as "Mom", but to others she can be what they know her as. Continuing with the phone number example, assuming we trust the phone number system (and yes, there are good reasons not to), you can trust your naming system as these are given by you and stored locally on your phone.

The maybe obvious problem with this is those times you get a phone call from a number you don’t know. Suddenly you have nothing to go off, who knows who could be phoning you. Also as we know from plenty of scam phone calls, sometimes even if you answer and they introduce themselves, can we really trust they are who they say they are?

# What is a petname system?

Well, it's a system that uses three different type of names, each type needs to be easily distinguishable from the other types and the different types together build a system which can work well in peer-to-peer systems as well as relying on trust you have already built. The three types of names are:

- **Self proposed names**: We've seen these above, it's names given to you by yourself (think of the chatroom example).

- **Petnames**: We've also seen these above, it's a name you give someone (think of the phone contact list).

- **Edgenames**: Names someone else has given someone with the intent of sharing them with others.

You’ll notice that two of the three types of names we are already familiar with, however with this system we get the benefit of having them both - reducing the downsides of each because you can easily discern a self proposed name from a petname.

We also have this new ”edgename” type, this might be better phrased as a ”shared name” or ”network name”. This is a name which you set for a person and then when you choose to share your edgenames with someone else, they get to know that you think this person goes by the edgename you set. In life we’re building up our own networks of trust between family, friends and businesses. This could be trusting someone a lot, a little or actively distrusting them. Edgenames build upon this instinctual trust we have already built.

Edgenames can be provided not just by people but also your workspace, companies or even your local government could publish an edgename database of the local businesses. Think back to a chatroom again, if a new person enters the chat that you’ve not seen but maybe they’re not unknown to your wider circle of friends and family, you could look at edgenames shared with you to give you an idea of who they are and if there are multiple edgenames for a person, they can help corroborate that this person is who you think it is. Additionally, with edgenames it would be possible to treat even existing naming hubs like DNS or Twitter as edgename naming hubs, they are just no more privileged than any other participant in the system (aside from likely being highly recognized and referenced).

The power of using these three naming systems and being able to at a glance tell these names apart, allows you to take the strengths of each type of name. This is even more important in a distributed system like the types of networks built on top of Spritely Goblins, as they are peer-to-peer with no central authority. While most contemporary naming systems rely on a central trusted authority, the directions we are building upon are built on your own user-provided contact list petnames and your local sphere of intentionally shared edgenames.

# So, how does this look in practice?

[Goblin Chat](https://gitlab.com/spritely/goblin-chat) is a (very early, and definitely not optimized for aesthetics) chat prototype built on top of Spritely's [Goblins](https://spritelyproject.org/#goblins) library and with it we've built [Brux](https://spritelyproject.org/#brux). Brux handles identity management and contact management by using the petname system. I'm going to skip past the identity management side of things for this post and focus on contact management.

Goblin Chat uses the Brux library to support petnames. When you start Goblin Chat you need to provide a name for yourself, which is your self proposed name. You can also give both a petname to someone (your chosen display name for them) and an edgename (which you are suggesting to others as who you think this person is). These are distinguished graphically. The graphics here aren’t final, but they should give you an idea of how these names might be easily distinguishable at a glance. We have the name in a box, with self proposed names there’s a dashed line around the edge and the name itself is prefixed by a ”?”. A petname on the other hand has a solid line around it and has no prefix. Here’s how an example chat window looks:

![Main goblin chat window](/static/images/blog/goblin-chat-petnames.png)

You can see in this chat there is:

- Alice (petname)
- Bob (petname)
- Alice (self proposed name)
- Reader (self proposed-name, this is us)

These are three different people, an Alice we know and trust as Alice, a Bob who we also know and trust and another person called Alice, but we don’t know them, we’ve not given them a petname yet. There are a lot of Alices around, it’s probably just another person called Alice we’ve yet to meet, or it could, of course, be someone malicious pretending to be Alice. However the system has already distinguished the Alice we know from this unknown Alice.

Lets look a bit further at the unknown Alice, we can right click on their name and view their profile:

![Alice's profile](/static/images/blog/goblin-chat-unknown-alice-profile.png)

The most eye catching thing here is the little monster figure. This is a unique image for this person generated from their ID (which you can’t pick yourself) and a secret salt (random string of characters we can add to the ID) which only we know. This means this monster is always going to be the same for us as people’s IDs don’t change, but the other person doesn’t know and can’t control what monster is being shown for them (this is important because then people can't try to generate reliable vanity avatars for themselves, which is not this purpose, and cannot try to spoof another user by generating avatars which look similar). When people can have the same name, there needs to be a reliable way we can tell them apart, and the generated avatar monsters provide such a feature.

You can also see some other information here:

- Self proposed name (this is their name, they have set)
- Petname (this is blank because we’ve not given them one, we could write something here)
- The edgename, also blank as we’ve not given them one.

Below the image and the basic profile information, we can see ”shared edgenames”. These are all the edgenames that have been shared to us. If there were multiple, it’d have multiple listed here. There is however just the one from our friend Bob, we know it’s our friend Bob as it’s got the box with the solid line and no preceding question mark. He has put the edgename ”Mallet (do not trust)”. This is interesting. It seems Bob believes this person’s name is Mallet, so maybe we should already be suspicious that they’re going around saying they're ”Alice”. Bob has also included a note not to trust them. Well, I trust Bob, so I’m going to be very weary of this new ”Alice” who’s in the chat.

With the petname system, so far we’ve been able to distinguish our friend Alice against this new person and we’ve been able to see through edgenames that Bob, who we have built trust with doesn’t think this new ”Alice” is trustworthy.

It doesn’t have to just warn about bad actors though, we can also see Bob’s profile:

![Bob's profile](/static/images/blog/goblin-chat-bob-profile.png)

We can see he has the self proposed name of "Robert", we've given him the petname ”Bob” because that’s the name we call him and know him by and we’ve given him the edgename of "Robert S.”. Finally we can look at the edgenames and we have one from our friend Alice (note, not the unknown Alice) and they’ve said they think this person is "Robert Smith". If we hadn't have given this person a petname, that information from Alice could have helped us trust this person is really our friend Bob who we play games with on weekends.

You can see that through this system we can have the trust that we have with our local contacts on our phone, the benefit of relying on the network of people and organizations around us and still having that self proposed name to give us some hints when we don’t have those things, but we now can tell them apart and know we need to build trust anew with this person.

You can check [goblin chat](https://gitlab.com/spritely/goblin-chat) out and play around with the new petname system, be aware that goblin chat is still alpha software so expect some rough edges and bugs.
